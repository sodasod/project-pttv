const { toRoman} = require('./roman.js')

describe('toRoman', () => {
it ('returns I when given 1', () => {
  expect(toRoman(1)).toEqual('I')
})
})
